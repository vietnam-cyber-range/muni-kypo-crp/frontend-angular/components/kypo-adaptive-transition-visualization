16.0.0 Update to Angular 16 and update local issuer to keycloak.
15.0.0 Update to Angular 15
14.0.2 Add export of access phase and task from public api.
14.0.1 Fix markdown after Sentinel update.
14.0.0 Update to Angular 14
13.0.0 Update to Angular 13, CI/CD optimisation, access level added, label overlap fixed
12.0.2 Remove unused methods
12.0.1 Fix lint errors
12.0.0 Initial version.
