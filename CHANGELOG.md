### 16.0.0 Update to Angular 16 and update local issuer to keycloak.
* 4cddb68 -- [CI/CD] Update packages.json version based on GitLab tag.
* 7762a45 -- Merge branch '14-update-to-angular-16' into 'master'
* fdde7dd -- Update to Angular 16
### 15.0.0 Update to Angular 15
* 73f25b8 -- [CI/CD] Update packages.json version based on GitLab tag.
* 61dd722 -- Merge branch '13-update-to-angular-15' into 'master'
* 9ed7864 -- Add json-server and its data
* b7d0814 -- Update to Angular 15
### 14.0.2 Add export of access phase and task from public api.
* 145c1b1 -- [CI/CD] Update packages.json version based on GitLab tag.
* fd1cf16 -- Merge branch '11-export-access-phase-and-task-model' into 'master'
* 3965c7e -- Resolve "Export access phase and task model"
### 14.0.1 Fix markdown after Sentinel update.
* 5f5b6e4 -- [CI/CD] Update packages.json version based on GitLab tag.
* b3844b2 -- Merge branch '10-fix-markdown-after-update' into 'master'
* 44b578a -- Resolve "Fix markdown after update"
### 14.0.0 Update to Angular 14
* beefc82 -- [CI/CD] Update packages.json version based on GitLab tag.
* 627002f -- Merge branch '9-update-to-angular-14' into 'master'
* 963c98b -- Resolve "Update to Angular 14"
### 13.0.0 Update to Angular 13, CI/CD optimisation, access level added, label overlap fixed
* ea337b2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   790fac5 -- Merge branch '7-update-to-angular-13' into 'master'
|\  
| * d2e957b -- Resolve "Update to Angular 13"
|/  
*   411b514 -- Merge branch '5-add-new-level-type-access-level' into 'master'
|\  
| * af04101 -- Added a new level type - access level.
|/  
*   ef6ba0b -- Merge branch '6-fix-label-overlap' into 'master'
|\  
| * 7678a26 -- Resolve "Fix label overlap"
|/  
* 44294b5 -- Merge branch '4-adjust-visualization-for-adaptive-model-simulator' into 'master'
* 78fad56 -- Resolve "Adjust visualization for adaptive model simulator"
### 12.0.2 Remove unused methods
