# KYPO Adaptive Transition Visualization

## Steps to Build & Develop

1.  Run `npm install`.
2.  Install json-server `npm install -g json-server`.
3.  Run the server with provided parameters `npm run api`.
4.  Run the app in local environment and ssl `npm start`
5.  Navigate to `https://localhost:4200/`. The app will automatically reload if you change any of the source files.
