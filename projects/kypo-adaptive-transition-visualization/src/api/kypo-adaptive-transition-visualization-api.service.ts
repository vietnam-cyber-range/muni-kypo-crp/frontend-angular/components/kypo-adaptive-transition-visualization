import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { VisualizationDataDTO } from '../dto/visualization-data-dto';
import { ConfigService } from '../config/config.service';
import { VisualizationDataMapper } from '../mappers/visualization-data-mapper';
import { VisualizationData } from '../model/visualization-data';

@Injectable({
  providedIn: 'root',
})
export class KypoAdaptiveTransitionVisualizationApi {
  constructor(private http: HttpClient, private configService: ConfigService) {}

  getDataForTrainingInstance(trainingInstanceId: number): Observable<VisualizationData> {
    return this.http
      .get<VisualizationDataDTO>(
        this.configService.config.trainingServiceUrl +
          `visualizations/training-instances/${trainingInstanceId}/transitions-graph`
      )
      .pipe(map((response) => VisualizationDataMapper.fromDTO(response)));
  }

  getDataForTrainingRun(trainingRunId: number): Observable<VisualizationData> {
    return this.http
      .get<VisualizationDataDTO>(
        this.configService.config.trainingServiceUrl + `visualizations/training-runs/${trainingRunId}/transitions-graph`
      )
      .pipe(map((response) => VisualizationDataMapper.fromDTO(response)));
  }
}
