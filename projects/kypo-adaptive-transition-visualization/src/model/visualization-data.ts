import { TransitionPhase } from './phase/transition-phase';
import { TrainingRunData } from './training-run-data';

export class VisualizationData {
  phases!: TransitionPhase[];
  trainingRunsData!: TrainingRunData[];
}
