import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import * as d3 from 'd3';
import { Task } from '../../../model/phase/task';
import { TransitionPhase } from '../../../model/phase/transition-phase';
import { InfoPhaseTask } from '../../../model/phase/info-phase/info-phase-task';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'kypo-info-task-preview',
  templateUrl: './info-task-preview.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoTaskPreviewComponent {
  @Input() task?: InfoPhaseTask;
}
