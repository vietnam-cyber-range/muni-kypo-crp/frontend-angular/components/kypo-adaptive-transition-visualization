import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import * as d3 from 'd3';
import { Task } from '../../../model/phase/task';
import { TransitionPhase } from '../../../model/phase/transition-phase';
import { TrainingPhaseTask } from '../../../model/phase/training-phase/training-phase-task';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'kypo-training-task-preview',
  templateUrl: './training-task-preview.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingTaskPreviewComponent {
  @Input() task?: TrainingPhaseTask;
}
