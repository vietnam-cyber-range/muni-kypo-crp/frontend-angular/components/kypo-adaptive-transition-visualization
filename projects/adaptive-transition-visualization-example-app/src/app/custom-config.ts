import { environment } from '../environments/environment';
import { AdaptiveTransitionVisualizationConfig } from '../../../kypo-adaptive-transition-visualization/src/public-api';

export const CustomConfig: AdaptiveTransitionVisualizationConfig = {
  trainingServiceUrl: environment.trainingServiceUrl,
};
