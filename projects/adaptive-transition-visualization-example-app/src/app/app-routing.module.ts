import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SentinelAuthProviderListComponent } from '@sentinel/auth/components';
import { SentinelAuthGuardWithLogin, SentinelNegativeAuthGuard } from '@sentinel/auth/guards';

const routes: Routes = [
  {
    path: 'transitions',
    loadChildren: () => import('./components/visualization/visualization.module').then((m) => m.VisualizationModule),
    //canActivate: [SentinelAuthGuardWithLogin],
  },
  {
    path: '',
    redirectTo: 'transitions',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: SentinelAuthProviderListComponent,
    canActivate: [SentinelNegativeAuthGuard],
  },
  {
    path: '**',
    redirectTo: 'transitions',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
